{ pkgs, lib, rustPlatform, ... }:
rustPlatform.buildRustPackage rec {
  pname = "weylus";
  version = "0.11.2";

  src = builtins.fetchGit {
    url = "https://github.com/H-M-H/Weylus.git";
    ref = "master";
    rev = "a89b7f481b82e96b1ae3e1586e683e22f7fefffa";
  };

  cargoBuildFlags = [ "--features=ffmpeg-system" ];
  cargoTestFlags = [ "--features=ffmpeg-system" ];

  cargoSha256 = "06245b9vh8ij19awr1kdi40867sccag56q6rv11yqwq1ik79z7jn";

  nativeBuildInputs = with pkgs; [
    cmake
    gcc
    rustc
    cargo
    nodePackages.typescript
    pkg-config
  ];

  buildInputs = with pkgs; [
    dbus
    ffmpeg
    gst_all_1.gst-plugins-base
    libdrm
    libva
    openssl
    xorg.libXcomposite
    xorg.libXcursor
    xorg.libXft
    xorg.libXinerama
    xorg.libXrandr
    xorg.libXtst
    xorg.libX11
    xorg.libXext
    xorg.libXi
    x264
  ];

  dbus = pkgs.dbus;

  meta = with lib; {
    description = "Weylus turns your tablet or smart phone into a graphic tablet/touch screen for your computer!";
    homepage = "https://github.com/H-M-H/Weylus";
    license = licenses.agpl3;
    platforms = platforms.all;
  };
}
