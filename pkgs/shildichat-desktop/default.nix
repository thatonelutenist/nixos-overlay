{ lib
, fetchFromGitHub
, makeWrapper
, makeDesktopItem
, mkYarnPackage
, electron
, schildichat-web
, python3
, sqlcipher
, rustc
, cargo
, pkg-config
, libsecret
, yarn
, rustPlatform
, callPackage
}:

let
  executableName = "schildichat-desktop";
in
mkYarnPackage rec {
  pname = "schildichat-desktop";
  inherit (schildichat-web) version;
  name = "${pname}-${version}";
  src = schildichat-web.src + "/element-desktop";

  packageJSON = ./package.json;
  yarnLock = ./yarn.lock;
  yarnNix = ./yarn.nix;

  nativeBuildInputs = [
    makeWrapper
    python3
    rustc
    cargo
    rustPlatform.cargoSetupHook
    pkg-config
    yarn
    sqlcipher
    libsecret # FIXME this should be in buildInputs
  ];

  patches = [
    ./0001-remove-menu-bar.diff
  ];

  postPatch =
    let
      inherit ((lib.importJSON packageJSON).build) electronVersion;
    in
    ''
      cargoRoot=../..$node_modules/matrix-seshat/native

      export HOME=/tmp
      mkdir -p $HOME/.electron-gyp/${electronVersion}
      tar -x -C $HOME/.electron-gyp/${electronVersion} --strip-components=1 -f ${electron.headers}
      echo 9 > $HOME/.electron-gyp/${electronVersion}/installVersion
    '';

  cargoDeps = rustPlatform.fetchCargoTarball {
    src = (lib.findFirst (x: lib.hasPrefix "matrix_seshat" x.name) null (callPackage yarnNix { }).packages).path;
    sourceRoot = "package/native";
    name = "sesha-cargo-deps";
    sha256 = "1f6hsw6rafs3hnqhrm135and0vrqy0qbda43bgcl4587sz1gzxm7";
  };

  preBuild = ''
    ls -l deps
    hak_modules=$(ls deps/schildichat-desktop/hak)
    (
      cd deps/schildichat-desktop

      node scripts/hak/index.js check

      mkdir -p .hak/hakModules
      for hak_module in $hak_modules
      do
        mkdir -p .hak/$hak_module
        cp -r $node_modules/$hak_module .hak/$hak_module/build
        chmod u+w -R .hak/$hak_module/build
        cp -r $node_modules/$hak_module .hak/hakModules/$hak_module
        chmod u+w -R .hak/hakModules/$hak_module
        rm -rf .hak/$hak_module/build/node_modules
        ln -s $node_modules .hak/$hak_module/build/node_modules
      done

      node scripts/hak/index.js build
      node scripts/hak/index.js copy

      for hak_module in $hak_modules
      do
        rm -rf .hak/$hak_module
      done

      mv .hak/hakModules ../..
      rm -rf .hak
    )
  '';

  installPhase = ''
    # resources
    mkdir -p "$out/share/element"
    ln -s '${schildichat-web}' "$out/share/element/webapp"
    cp -r './deps/schildichat-desktop' "$out/share/element/electron"
    cp -r './deps/schildichat-desktop/res/img' "$out/share/element"
    rm "$out/share/element/electron/node_modules"
    cp -r './node_modules' "$out/share/element/electron"

    # native modules
    for hak_module in $hak_modules
    do
      rm -rf $out/share/element/electron/node_modules/$hak_module
      mv hakModules/$hak_module $out/share/element/electron/node_modules
    done

    # icons
    for icon in $out/share/element/electron/build/icons/*.png; do
      mkdir -p "$out/share/icons/hicolor/$(basename $icon .png)/apps"
      ln -s "$icon" "$out/share/icons/hicolor/$(basename $icon .png)/apps/element.png"
    done

    # desktop item
    mkdir -p "$out/share"
    ln -s "${desktopItem}/share/applications" "$out/share/applications"

    # executable wrapper
    makeWrapper '${electron}/bin/electron' "$out/bin/${executableName}" \
      --add-flags "$out/share/element/electron --enable-features=UseOzonePlatform --ozone-platform=wayland"
  '';

  # Do not attempt generating a tarball for element-web again.
  # note: `doDist = false;` does not work.
  distPhase = ''
    true
  '';

  desktopItem = makeDesktopItem {
    name = "schildichat-desktop";
    exec = "schildichat-desktop %u";
    icon = "schildichat";
    desktopName = "SchildiChat";
    genericName = "Matrix Client";
    categories = "Network;InstantMessaging;Chat;";
    extraEntries = ''
      StartupWMClass=schildichat
      MimeType=x-scheme-handler/element;
    '';
  };
}
