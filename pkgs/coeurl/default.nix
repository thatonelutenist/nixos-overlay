{ lib, stdenv, mkDerivation, fetchFromGitLab, curl, libevent, spdlog, cmake }:
mkDerivation rec {
  pname = "coeurl";
  version = "1.0.0-dev";
  src = fetchFromGitLab {
    owner = "Nheko-Reborn";
    repo = "coeurl";
    rev = "22f58922da16c3b94d293d98a07cb7caa7a019e8";
    sha256 = lib.fakeHash;
  };

  nativeBuildInputs = [
    cmake
  ];

  buildInputs = [
    curl
    libevent
    spdlog
  ];

  meta = with lib; {
    description = "Simple library to do http requests asynchronously via CURL in C++.";
    homepage = "https://nheko.im/nheko-reborn/coeurl";
    maintainers = with maintainers; [ ekleog fpletz ];
    platforms = platforms.all;
    # Should be fixable if a higher clang version is used, see:
    # https://github.com/NixOS/nixpkgs/pull/85922#issuecomment-619287177
    broken = stdenv.targetPlatform.isDarwin;
    license = licenses.gpl3Plus;
  };
}
