{ lib, fetchFromGitHub, python3Packages, mopidy, gst_all_1 }:

python3Packages.buildPythonApplication rec {
  pname = "mopidy-youtube";
  version = "3.4";

  src = fetchFromGitHub {
    owner = "natumbri";
    repo = "mopidy-youtube";
    rev = "d4f4bb07fea29eb8f2ff85683dab64b4aa8ea9f6";
    sha256 = "1l2jqgaamnma0k51albnybarffgcznshk1rrcnz4hyd6763nn0bs";
  };

  patchPhase = "sed s/bs4/beautifulsoup4/ -i setup.cfg";

  propagatedBuildInputs = [
    gst_all_1.gstreamer
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-ugly
    mopidy
    python3Packages.beautifulsoup4
    python3Packages.cachetools
    python3Packages.youtube-dl
    python3Packages.ytmusicapi
  ];

  doCheck = false;

  meta = with lib; {
    description = "Mopidy extension for playing music from YouTube";
    license = licenses.asl20;
    maintainers = [ maintainers.spwhitt ];
  };
}
