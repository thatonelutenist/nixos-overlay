{ pkgs ? import <nixpkgs> {
    overlays = [ (import ./overlay.nix) ];
  }
}:
pkgs.mkShell {
  # nativeBuildInputs is usually what you want -- tools you need to run
  nativeBuildInputs = [ pkgs.discord-electron pkgs.weylus ];
}
