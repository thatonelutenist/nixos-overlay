{ pkgs ? import <nixpkgs> { } }:
let
  inherit (pkgs) callPackage fetchurl;
  inherit (pkgs.callPackages ./pkgs/electron { }) electron_nm;
in
{

  discord-wayland = callPackage ./pkgs/discord/default.nix rec {
    pname = "discord-electron";
    binaryName = "Discord";
    desktopName = "Discord (Wayland)";
    version = "0.0.16";
    src = fetchurl {
      url = "https://dl.discordapp.net/apps/linux/${version}/discord-${version}.tar.gz";
      sha256 = "1s9qym58cjm8m8kg3zywvwai2i3adiq6sdayygk2zv72ry74ldai";
    };
    electron = electron_nm;
  };

  weylus = callPackage ./pkgs/weylus/default.nix { };

  mopidy-youtube = callPackage ./pkgs/mopidy-youtube/default.nix { };

}
