self: super:
let
  mypkgs = import ./default.nix { pkgs = super; };
in
{
  discord-wayland = mypkgs.discord-wayland;
  weylus = mypkgs.weylus;
  nheko = mypkgs.nheko;
  mpdyoutube = mypkgs.mopidy-youtube;
}
